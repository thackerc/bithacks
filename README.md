# Bithacks

## 2s Compliment
- https://www.youtube.com/watch?v=4qH4unVtJkE
- https://stackoverflow.com/questions/1049722/what-is-2s-complement

## C Operators

What is `sizeof(int) * CHAR_BIT`?

- `sizeof()` returns the size of a variable.
- `CHAR_BIT` is the number of bits in char. These days, almost all architectures use 8 bits per byte. 
  https://www.geeksforgeeks.org/char_bit-in-c/
- https://www.tutorialspoint.com/cprogramming/c_operators.htm



# Cheatsheet

| Oper­ation    | Exam­ple | Input | Output |
| ------------ |:-------------:| -------------:| -----:|
| NOT | x = ~0111; |  | 1000 |
| AND | x = 0101 & 0011; |  | 1 |
| OR | x = 0101 \| 0011; |  | 111 |
| XOR | x = 0101 ^ 0011; |  | 110 |
| Left Shift | x = 0100 << 1; |  | 1000 |
| Right Shift | x = 0100 >> 1; |  | 10 |
| Set bit 5 | x \|= (1<­<5); | 0b00000000 | 0b00100000 |
| Clear bit 5 | x &= ~(1<<5); | 0b11111111 | 0b11011111 |
| Wait until bit 5 is set | while (­!(x & (1<­­<5))); |  |  |
| Wait until bit 5 is cleared | while (x & (1<­­<5)); |  |  |
| Save value of bit 5 into variable | int var = x & (1<­­<5); |  |  |
| Test if bit 5 is set | if (x & (1<­­<5)) {...} |  |  |
| Toggle bit 5 | x ^= (1<­­<5); | 0b00000000 | 0b00100000 |
| Replace modulo of power of two with AND | x % y == x & (y -1) | x % 64 | x & (63) |
| Check if integer x is odd | if (x & 1) { ... } |  |  |
| Turn off the rightmost 1-bit | x = x & (x-1); | 0b01011000 | 0b01010000 |
| Isolate the rightmost 1-bit | x = x & (-x); | 0b01110000 | 0b00010000 |
| Right propagate the rightmost 1-bit | x = x \| (x-1); | 0b10111100 | 0b10111111 |
| Isolate the rightmost 0-bit | x = ~x & (x+1); | 0b01110111 | 0b00001000 |
| Turn on the rightmost 0-bit. | x = x \| (x+1); | 0b01110111 | 0b01111111 |
| Right propagate the rightmost 0-bit | x = x & (x+1); | 0b01110111 | 0b01110000 |
| Multiply by 2 | x <<= 1; | 0b00000010 | 0b00000100 |
| Divide by 2 | x >>= 1; | 0b00000010 | 0b00000001 |
| XOR swap | a ^= b; b ^= a; a ^= b; |  |  |
| Calculate 2^n | 1 << n; |  |  |
| Convert letter to lowercase | x = (x \| ' '); | A | a |
| Convert letter to uppercase | x = (x & '_'); | a | A |
| Swap Nibbles | x = (x << 4) \| (x >> 4); | 0b11110000 | 0b00001111 |

Taken from: https://www.cheatography.com/miracoli/cheat-sheets/bitmanipulation/


# Examples

## Swap two integers without a third variable
```C
int x = 10, y = 5;

// Code to swap ‘x’ and ‘y’
x = x + y; // x now becomes 15
y = x – y; // y becomes 10
x = x – y; // x becomes 5

printf(“After Swapping: x = %d, y = %d”, x, y);
```

## Detect if two integers have opposite signs
```C
int x, y;               // input values to compare signs
bool f = ((x ^ y) < 0); // true iff x and y have opposite signs
```


## Compute the integer absolute value (abs)

Some CPUs don't have an integer absolute value instruction (or the compiler fails to use them). The obvious approach: 

```C
r = (v < 0) ? -(unsigned)v : v
```

On machines where branching is expensive (same number of operations)

```C
int v;           // we want to find the absolute value of v
unsigned int r;  // the result goes here 
int const mask = v >> sizeof(int) * CHAR_BIT - 1;
r = (v + mask) ^ mask;

// Patented variation:
r = (v ^ mask) - mask;
```



## Compute the minimum (min) of two integers without branching
```C
// we want to find the minimum of x and y
int x;  
int y;   
int r;  // the result goes here 

r = y ^ ((x ^ y) & -(x < y)); // min(x, y)
```
Example. x = 4, y = 12.
```
                       x    00000100 = 4
                       y    00001100 = 12
                   x < y    00000001 = true
                   x ^ y    00001000
                -(x < y)    11111111
      (x ^ y) & -(x < y)    00001000
y ^ ((x ^ y) & -(x < y))    00000100 = 4
```

On some rare machines where branching is very expensive and no condition move instructions exist, the above expression might be faster than the obvious approach, `r = (x < y) ? x : y`, even though it involves two more instructions. (Typically, the obvious approach is best, though.) It works because if `x < y`, then `-(x < y)` will be all ones, so `r = y ^ (x ^ y) & ~0 = y ^ x ^ y = x`. Otherwise, if `x >= y`, then `-(x < y)` will be all zeros, so `r = y ^ ((x ^ y) & 0) = y`. On some machines, evaluating `(x < y)` as `0` or `1` requires a branch instruction, so there may be no advantage.

Quick and dirty version:
If you know that INT_MIN <= x - y <= INT_MAX, then you can use the following, which are faster because (x - y) only needs to be evaluated once.
```C
r = y + ((x - y) & ((x - y) >> (sizeof(int) * CHAR_BIT - 1))); // min(x, y)
```


# Demos
## hello.c

Use these commands to compile and run your hello world.
> gcc -o hello hello.c

> ./hello

## max.c
> clear && gcc -std=c99 -o max max.c && ./max

# Challenges
1. sign.c (10 min)
1. pow.c (10 min)
1. parity.c (15 min, open Google)


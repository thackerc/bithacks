#include <limits.h>
#include <stdio.h>
#include <stdlib.h> 
#include <sys/time.h>
#include <time.h>


// max(x, y)
int task(int x, int y) { 
  return x ^ ((x ^ y) & -(x < y));
} 



///// DON"T EDIT THE CODE BELOW THIS POINT

void taskTest() {

  int tests = 4;
  int score = 0;

  int xValues[4] = { 1, 1, -976, -9856984 };
  int yValues[4] = { 1, 2, -576, 0 };
  int eValues[4] = { 1, 2, -576, 0 };

  int x, y, e, r;

  for(int i = 0; i < tests; ++i) {
    x = xValues[i];
    y = yValues[i];
    e = eValues[i];
    r = task(x, y);

    printf("x: %8d, y: %4d, e: %4d r: %4d\n", x, y, e, r);

    if(r == e) {
      score++;
    }
  } 

  printf("\n");
  printf("%d of %d tests passed\n", score, tests);
}

void taskAvgTime(int runs) {

  srand(time(NULL));
  unsigned long runningTime = 0;

  int xValues[runs];
  int yValues[runs];
  for (int i = 0; i < runs; i++) {
    xValues[i] = rand() % 20000 - 10000;
    yValues[i] = rand() % 20000 - 10000;
  }

  struct timeval stop, start;
  gettimeofday(&start, NULL);
  for (int i = 0; i < runs; i++) {
    task(xValues[i], yValues[i]);
  }
  gettimeofday(&stop, NULL);
  runningTime = (stop.tv_usec - start.tv_usec);


  double time_taken = ((double) runningTime) / runs;
  printf("Task Avg Time: %f us\n", time_taken);
}

int main(int argc, char *argv[]) {
  taskTest();
  taskAvgTime(1000000);

  return 0;
}




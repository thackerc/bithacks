#include <limits.h>
#include <stdio.h>
#include <stdlib.h> 
#include <sys/time.h>
#include <time.h>
#include <stdbool.h>


// Return true if x is a power of 2.
bool task(int x) { 
  bool result;



  return result;
} 



///// DON"T EDIT THE CODE BELOW THIS POINT

void taskTest() {

  int tests = 12;
  int score = 0;

  int xValues[12] = { 1, 2, 3, 4, 9, 24, 1024, 1048576, 0, -1, -2, -9856984 };
  bool eValues[12] = { 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0 };

  int x, e, r;

  for(int i = 0; i < tests; ++i) {
    x = xValues[i];
    e = eValues[i];
    r = task(x);

    printf("x: %8d, e: %2d r: %2d\n", x, e, r);
    if(r == e) {
      score++;
    }
  } 

  printf("\n");
  printf("%d of %d tests passed\n", score, tests);
}

void taskAvgTime(int runs) {

  srand(time(NULL));
  unsigned long runningTime = 0;

  int xValues[runs];
  for (int i = 0; i < runs; i++) {
    xValues[i] = rand() % 20000 - 10000;
  }

  int x =  rand() % 20000 - 10000;

  struct timeval stop, start;
  gettimeofday(&start, NULL);
  for (int i = 0; i < runs; i++) {
    task(xValues[i]);
  }
  gettimeofday(&stop, NULL);
  runningTime = (stop.tv_usec - start.tv_usec);

  double time_taken = ((double) runningTime) / runs;
  printf("Task Avg Time: %f us\n", time_taken);
}

int main(int argc, char *argv[]) {
  taskTest();
  taskAvgTime(1000000);

  return 0;
}



